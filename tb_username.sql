USE [StandByDB]
GO

/****** Object:  Table [dbo].[tb_user]    Script Date: 31/7/2563 18:03:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[username] [varchar](100) NULL,
	[first_name] [varchar](100) NULL,
	[last_name] [varchar](100) NULL,
	[create_by] [varchar](100) NULL,
	[create_date] [datetime] NULL,
	[last_update_by] [varchar](100) NULL,
	[last_update_date] [datetime] NULL
) ON [PRIMARY]
GO

