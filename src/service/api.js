import axios from "axios";
if (process.env.NODE_ENV === `development`) {
    var path = "";
}
else{
    // var path = "http://bbtecdemo.onthewifi.com";
}

function Get(url, params) {
    function onSuccess(success) {
        return success;
    }
    function onError(error) {
        return error;
    }
    try {
        const success = axios.get(
            path + "/maxioperationapi/api/"+ url + "/" + params
        );
        return onSuccess(success);
    } catch (error) {
        return onError(error);
    }
}

function Post(url, params) {
    function onSuccess(success) {
        return success;
    }
    function onError(error) {
        return error;
    }
    try {
        const success = axios.post(
            path + "/maxioperationapi/api/"+ url , params
        );
        return onSuccess(success);
    } catch (error) {
        return onError(error);
    }
}

export {
    Get,
    Post
}

