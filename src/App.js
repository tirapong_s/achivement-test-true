import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import MaxiOperation from './pages/index'
const NotFoundPage = () => <div>not</div>

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            active: false
        };
    }

    componentDidMount () {
        
    }

    render() {
        return (
            <Switch>
                <Route exact path="/TestTrue" component={MaxiOperation} />
            
                <Route component={NotFoundPage} />
            </Switch>
        )
    }
}

export default App
